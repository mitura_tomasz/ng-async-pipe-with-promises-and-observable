import { Component, OnInit } from '@angular/core';
import { Observable, Subscription, interval, pipe } from 'rxjs';
import { map, take } from 'rxjs/operators';

@Component({
  selector: 'app-observable-async-method',
  templateUrl: './observable-async-method.component.html',
  styleUrls: ['./observable-async-method.component.scss']
})
export class ObservableAsyncMethodComponent implements OnInit {

  observableAsyncData: Observable<number>;

  constructor() { }

  ngOnInit() {
    this.observableAsyncData = this.getObservable();
  }

   getObservable(): any { 
    return interval(1000).pipe(
      take(10), // 'take' will take the first n number of emissions 
      map((number) => number * number)
    ) 
  }
}
