import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-async-first-method',
  templateUrl: './async-first-method.component.html',
  styleUrls: ['./async-first-method.component.scss']
})
export class AsyncFirstMethodComponent implements OnInit {

  promiseData: string;

  constructor() { }

  ngOnInit() {
    this.getPromise().then(v => this.promiseData = v);
  }

  getPromise(): Promise<any> {
    return new Promise((resolve, reject) => {
      setTimeout(() => resolve('Promise complete!'), 3000);
    });
  }

}
