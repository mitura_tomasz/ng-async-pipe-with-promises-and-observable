import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-async-second-method',
  templateUrl: './async-second-method.component.html',
  styleUrls: ['./async-second-method.component.scss']
})
export class AsyncSecondMethodComponent implements OnInit {

  promise: Promise<string>;

  ngOnInit() {
    this.promise = this.getPromise();
  }

  getPromise(): Promise<any> {
    return new Promise((resolve, reject) => {
      setTimeout(() => resolve('Promise complete!'), 3000);
    });
  }

}
