import { Component, OnInit } from '@angular/core';
import { Observable, Subscription, interval, pipe } from 'rxjs';
import { map, take } from 'rxjs/operators';

@Component({
  selector: 'app-observable-method',
  templateUrl: './observable-method.component.html',
  styleUrls: ['./observable-method.component.scss']
})
export class ObservableMethodComponent implements OnInit {

  observableData: number = 0;

  constructor() { }

  ngOnInit() {
     this.subscribeObservable();
  }

   getObservable(): any { 
    return interval(1000).pipe(
      take(10), // 'take' will take the first n number of emissions 
      map((number) => number * number)
    ) 
  }

  subscribeObservable(): void { 
    this.getObservable().subscribe( resNumber => this.observableData = resNumber);
  }

}
