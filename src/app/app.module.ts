import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AsyncFirstMethodComponent } from './components/async-first-method/async-first-method.component';
import { AsyncSecondMethodComponent } from './components/async-second-method/async-second-method.component';
import { ObservableMethodComponent } from './components/observable-method/observable-method.component';
import { ObservableAsyncMethodComponent } from './components/observable-async-method/observable-async-method.component';

@NgModule({
  declarations: [
    AppComponent,
    AsyncFirstMethodComponent,
    AsyncSecondMethodComponent,
    ObservableMethodComponent,
    ObservableAsyncMethodComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
