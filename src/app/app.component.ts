import { Component } from '@angular/core';
import { Observable, Subscription, interval, pipe } from 'rxjs';
import { map, take } from 'rxjs/operators';
// import { timer } from 'rxjs/add/observable/timer';
import 'rxjs/add/observable/timer'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
 
  seconds: number = 0;

  constructor() {
    this.runTimer();
  }

  runTimer(): void {
    let timer = Observable.timer(0, 1000);
    timer.subscribe(seconds => {
      if (this.seconds < 3) {
        this.seconds = seconds
      }
    });
  }

}
